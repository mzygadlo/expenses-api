package com.mzygadlo.expences.repositories;

import org.springframework.data.repository.CrudRepository;
import com.mzygadlo.expences.models.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
