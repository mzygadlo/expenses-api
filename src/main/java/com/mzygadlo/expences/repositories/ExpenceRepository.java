package com.mzygadlo.expences.repositories;

import org.springframework.data.repository.CrudRepository;
import com.mzygadlo.expences.models.Expence;


public interface ExpenceRepository extends CrudRepository<Expence,Long>{
}