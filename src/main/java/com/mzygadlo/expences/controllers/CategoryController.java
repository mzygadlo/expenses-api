package com.mzygadlo.expences.controllers;

import java.util.Optional;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mzygadlo.expences.models.Category;
import com.mzygadlo.expences.repositories.CategoryRepository;

@CrossOrigin(origins = "https://expences-app.herokuapp.com")
@RestController
@RequestMapping("/categories")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping()
	public Iterable<Category> findAll() {
		return categoryRepository.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Category> findByID(@PathVariable @NotNull @DecimalMin("0") Long id) {
		return categoryRepository.findById(id);
	}

	@PostMapping()
	public Category addCategory(@RequestBody Category category) {
		return categoryRepository.save(category);
	}

	@PutMapping("/{id}")
	public Category editByID(@PathVariable Long id, @RequestBody Category newCategory) {
		Category category = categoryRepository.findById(id).get();
		category.setName(newCategory.getName());
		return categoryRepository.save(category);
	}

	@DeleteMapping("/{id}")
	public void deleteByID(@PathVariable @NotNull @DecimalMin("0") Long id) {
		categoryRepository.deleteById(id);
	}
}
