package com.mzygadlo.expences.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mzygadlo.expences.models.Expence;
import com.mzygadlo.expences.repositories.ExpenceRepository;

@CrossOrigin
@RestController
@RequestMapping("/expences")
public class ExpenceController {

	@Autowired
	private ExpenceRepository expenceRepository;

	@GetMapping()
	public Iterable<Expence> findAll() {
		return expenceRepository.findAll();
	}

	@GetMapping("/{id}")
	public Expence findById(@PathVariable @NotNull @DecimalMin("0") Long id) {
		return expenceRepository.findById(id).get();
	}

	@PostMapping()
	public Expence addExpence(@RequestBody Expence expence) {
		return expenceRepository.save(expence);
	}

	@PutMapping("/{id}")
	public Expence editByID(@PathVariable @NotNull @DecimalMin("0") Long id, @RequestBody Expence expence) {
		Expence newExpence = expenceRepository.findById(id).get();
		newExpence.setCost(expence.getCost());
		newExpence.setDate(expence.getDate());
		newExpence.setCategory(expence.getCategory());
		return expenceRepository.save(newExpence);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable @NotNull @DecimalMin("0") Long id) {
		expenceRepository.deleteById(id);
	}

	@GetMapping("/day/{day}")
	public List<Expence> viewDay(@PathVariable String day) {
		List<Expence> expences = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(day, formatter);

		expenceRepository.findAll().forEach(expence -> {
			if (expence.getDate() != null && expence.getDate().compareTo(date) == 0) {
				expences.add(expence);
			}
		});
		return expences;
	}

	@GetMapping("/days/{dayFrom}/{dayTo}")
	public List<Expence> viewDays(@PathVariable String dayFrom, @PathVariable String dayTo) {
		List<Expence> expences = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateFrom = LocalDate.parse(dayFrom, formatter);
		LocalDate dateTo = LocalDate.parse(dayTo, formatter);

		expenceRepository.findAll().forEach(expence -> {
			if (expence.getDate() != null && expence.getDate().compareTo(dateFrom) >= 0
					&& expence.getDate().compareTo(dateTo) <= 0) {
				expences.add(expence);
			}
		});
		return expences;
	}

	@GetMapping("/category/{id}")
	public List<Expence> viewCategory(@PathVariable Long id) {
		List<Expence> expences = new ArrayList<>();
		expenceRepository.findAll().forEach(expence -> {
			if (expence.getCategory().getId() == id) {
				expences.add(expence);
			}
		});
		return expences;
	}

	@GetMapping("/category/{id}/day/{day}")
	public List<Expence> viewCategoryByDay(@PathVariable Long id, @PathVariable String day) {
		List<Expence> expences = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(day, formatter);

		expenceRepository.findAll().forEach(expence -> {
			if (expence.getDate() != null && expence.getDate().compareTo(date) == 0 && expence.getCategory() != null
					&& expence.getCategory().getId() == id) {
				expences.add(expence);
			}
		});
		return expences;
	}

	@GetMapping("/category/{id}/days/{dayFrom}/{dayTo}")
	public List<Expence> viewCategoryByDays(@PathVariable Long id, @PathVariable String dayFrom,
			@PathVariable String dayTo) {
		List<Expence> expences = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateFrom = LocalDate.parse(dayFrom, formatter);
		LocalDate dateTo = LocalDate.parse(dayTo, formatter);

		expenceRepository.findAll().forEach(expence -> {
			if (expence.getDate() != null && expence.getDate().compareTo(dateFrom) >= 0
					&& expence.getDate().compareTo(dateTo) <= 0 && expence.getCategory() != null
					&& expence.getCategory().getId() == id) {
				expences.add(expence);
			}
		});
		return expences;
	}

}
