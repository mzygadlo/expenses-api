# [expense-API](https://expences-api.herokuapp.com/categories/)
This is an API for application which helps you manage your personal expenses. You can check it by clicking [here](https://expences-api.herokuapp.com/categories/)

This API is used in my another application written in Angular 4 - you can visit it [here](https://expences-app.herokuapp.com)

## Technologies I use
- Maven 3;
- Java 8;
- Spring Boot, Spring Web, Spring JPA/Hibernate;
- PostgreSQL
- Heroku

## Description
There are two tabels: categories and expenses, connected by @ManyToOne annotation.

#### Category
Category model has only two properties: id and name.
The CategoryController gives you methods to get one or all categories, add a new one and update or delete them.

#### Expense
Expense is more complicated. It has: id, date, cost and id of category.
The ExpenseController has similar methods to CategoryController, but is is extended by searching by day, chosen period and category, and connection of the last two.
